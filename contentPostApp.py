import webApp

form = """
    <p>
    <form action="" method = "POST">
    Recurso: <input type="text" name="resource"><BR>
    <input type="submit" value="Enviar"> <input type="Reset">
    </form>
    </p>
"""


class ContentPOST(webApp.WebApp):
    """
    Class that serves the content stored in a Python dictionary
    """

    dict_apps = {"/": "<h1><h1>Esta es la p&aacute;gina principal</h1>"}

    def parse(self, received):
        """
        Returns the method, resource (including the /), and "body" of the resource requested by URL
        """
        method = received.split(' ')[0]
        resource = received.split(" ", 2)[1]
        if method == "POST":
            resource = '/' + received.split("resource=")[1]
            body = "<html><body><h1>Esta es la p&aacute;gina de: " + received.split("resource=")[1] + \
                   "<h1></html></body>"
        else:
            body = None
        return method, resource, body

    def process(self, analyzed):
        """
        Returns the http code and html text of each requested resource
        """
        method, resource, body = analyzed

        if method == "GET":  # If the method is GET, try to serve the resource
            if resource in self.dict_apps.keys():  # The resource is in the dictionary
                http = "200 OK"
                html = "<html><body>" + self.dict_apps[resource] + "<p> Si quieres a&ntilde;adir un recurso, " \
                                                                   "rellena el siguiente formulario: </p>" + form + \
                                                                   "</body></html>"
            else:
                http = "404 Not Found"
                html = "<html><body><h1>No tengo el recurso que me has pedido, cr&eacute;alo rellenando " \
                       "el formulario: </h1>" + form + "</html></body>"

        elif method == "POST":
            http = "200 OK"
            self.dict_apps[resource] = body
            html = "<html><body><h1>&iexcl;El recurso que me has enviado ha sido guardado " \
                   "correctamente!</h1></html></body>"

        else:
            http = "404 Not Found"
            html = "<html><body><h1>Eso no ha funcionado, aqu&iacute; no servimos con ese m&eacute;todo, " \
                   "prueba otra vez</h1></html></body>"

        return http, html


if __name__ == "__main__":
    testWebApp = ContentPOST("localhost", 1234)

